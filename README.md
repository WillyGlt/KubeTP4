TP4:

Créer un HELM Chart

Il doit générer :

- Un déploiement de serveur web
- Un déploiement de serveur MariaDB
- 2 PV et 2 PVC
- Un service pour accéder serveur web
- Un service pour que le serveur web puisse accéder à la base de données

Dans les values je dois pouvoir configurer :

- PV : name, size, path, persistentVolumeReclaimPolicy
- PVC : name, size, volumeName
- Serveur Web
    - volumeMount : name, mountPath
    - volumes : name, claimName
    - service : type, port, name
    - image : repository, pullPolicy, tag
    - replicaCount
- MariaDB
    - rootPassword, user, password, database
    - image : repository, pullPolicy, tag
    - volumeMount : name, mountPath
    - volumes : name, claimName
    - service : name

Quand on installe le chart il faut que ça affiche comment accéder aux serveurs web, quand on accède au serveur web (par le navigateur ou en curl ou autre ...) il faut que ça mette à jour les données dans la base de données et dans le PV et que ça nous les affiche. 
